package jojo;
import robocode.*;
import java.awt.Color;
import robocode.HitByBulletEvent;
import robocode.HitRobotEvent;
import robocode.WinEvent;
import robocode.Robot;
import robocode.DeathEvent;
import robocode.ScannedRobotEvent;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * JoeBot_v2 - a robot by Jordan dos Santos Jones
 */
public class JoeBot_v2 extends Robot
{
	/**
	 * run: JoeBot_v2's default behavior
	 */

	public void run() {

		// setColors(Color.red,Color.blue,Color.green); // body,gun,radar
		setBodyColor(new Color(0, 56, 168));
		setGunColor(new Color(214, 2, 112));
		setRadarColor(new Color(155, 79, 150));
		setScanColor(Color.red);
		setBulletColor(new Color(229, 166, 5));

		// Robot main loop
		while(true) {
			turnGunRight(360);
			turnLeft(90);			
			ahead(150);
			turnGunRight(360);
			turnLeft(90);
			back(150);
		}
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
		private int robotProximity = 60; 
		private int tooCloseToRobot = 10;
			
	public void onScannedRobot(ScannedRobotEvent e) {
		// demonstrate feature of debugging properties on RobotDialog
		setDebugProperty("lastScannedRobot", e.getName() + " at " + e.getBearing() + " degrees at time " + getTime());
		fire(1);
		scan();
		out.println(";-9 ");
		
		/**
		 * Condition implemented to mitigate fumbling next to enemies
		 */	
			if (tooCloseToRobot <= 0) {
				// if we weren't already dealing with the walls, we are now
				tooCloseToRobot += robotProximity;
				turnGunLeft(360);
				fire(1);
			}
		
	}
		
		/**
		 * Following custom event implemented to avoid hitting walls
		 */
		private int wallMargin = 60; 
		private int tooCloseToWall = 0;
	
	public void onCustomEvent(CustomEvent e) {
		if (e.getCondition().getName().equals("too_close_to_walls"))
		{
			if (tooCloseToWall <= 0) {
				// if we weren't already dealing with the walls, we are now
				tooCloseToWall += wallMargin;
			}
		}
	}

	/**
	 * onHitRobot: What to do after you shot another robot
	 */
	public void onHitRobot(HitRobotEvent e) {
	        stop();
			fire(1);	
	}
		
	// normalizes a bearing to between +180 and -180
	double normalizeBearing(double angle) {
		while (angle >  180) angle -= 360;
		while (angle < -180) angle += 360;
		return angle;
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		turnRight(60);
		ahead(100);
		out.println(";-( ");
		scan();			
	}
	
	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e) {
		turnRight(60);
		ahead(100);
	}
	
	/**
	 * onWin: If the battle is won:
	 */
	public void onWin(WinEvent e) {
		// Victory dance
		out.println(":-D ");
		turnRight(36000);
	}

	/**
	 * onDeath: If the battle is lost:
	 */
	public void onDeath(DeathEvent e) {
		turnGunLeft(36000);		
		out.println(":,( ");	
	}
	
}
