# JoeBot
Um projeto por Jordan dos Santos Jones para o Solutis Robot Arena - 2020

## Descrição:
Este é meu primeiro projeto utilizando o framework do robocode, através da linguagem Java. O robô em si é relativamente simples.

## Funções:
Ao início de cada round, JoeBot escanea a arena em busca de um oponente. Após isso, ele irá atirar no oponente escaneado, e ao acertar o tiro, ele para de se mover e atira novamente. Caso o oponente seja destruído ou se mova do local, JoeBot voltará a escanear a arena. 

## Pontos fortes:
- JoeBot detecta danos recebidos e automaticamente se move do local onde estava, se tornando difícil de atingir em longas distâncias.
- Poucas chances de ficar preso em paredes.
- Boas manobras evasivas.

## Pontos fracos:
- O código de JoeBot é um tanto básico, o tornando bastante previsível.
- JoeBot não lida muito bem com combates a curta distância.
- Baixo dano.

## Minhas experiências:
Embora alguns imprevistos na minha vida pessoal tenham adiado a entrega deste projeto para perto demais da data limite, a experiência foi bastante recompensadora ainda assim. Minhas especialidades em programação normalmente são Front-End e Web Design, então este projeto foi uma excelente forma de aprimorar minhas competências com a linguagem Java.